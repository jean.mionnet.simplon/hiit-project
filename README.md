# vue-hiit

## Installation des dépendances du projet
```
npm install
```

### Compile et lance le serveur de développement
```
npm run serve
```

### Compile et minifie pour la production
```
npm run build
```

### Linter
```
npm run lint
```